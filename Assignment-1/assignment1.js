
/*  Good work here Abla.  Your js app performs the validation
as required and follows all the business rules.  You've got
the comments I needed to see, so everything is good here.
20/20
*/

// the javascript page.
// this function loads the provinces array into our select box on the page
    function loadProvinces(){
        var ProvArray=["Ontario","Quebec","British Columbia","Alberta",
        "Nova Scotia","Saskatchewan","Manitoba","Newfoundland and Labrador",
        "New Brunswick","Prince Edward Island"];
        var output="<option value=''>-Select-</option>";
        for(index=0;index<ProvArray.length;index++){
            output+="<option value='"+ProvArray[index]+"'>"+ProvArray[index]+"</option>";
        }
        document.getElementById('cboProv').innerHTML=output;
    }

//this function validates the values and sets alerts when entered incorrectly
    function validateForm(){
        var name = document.getElementById('txtName').value;
        var email = document.getElementById('txtEmail').value;
        var province = document.getElementById('cboProv').value;

        if(name==""){
            alert('please enter a name');
            document.getElementById('txtName').focus();
            return false;
        }
        if(email==""){
            alert('please enter an email');
            document.getElementById('txtEmail').focus();
            return false;
        }

        if(province==""){
            alert('please select a province');
            document.getElementById('cboProv').focus();
            return false;
        }

        alert('congrats, you entered the form correctly');
    }
