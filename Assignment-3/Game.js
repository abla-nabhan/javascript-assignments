// Great work here, Abla.  Your
// game looks and runs nicely.  You've
// got everything named and calling things
// correctly as well.
// 20/20

// Global variables
var gamePieces;
var userChoice;
var computerChoice;

// Random index function
function getRandomGamePiece(arrayLength){
    // get my randome array index
    var rnd = Math.floor((Math.random() * arrayLength) + 0);
    // return our random index back
    return rnd;
}

// start game button initializes this function
function startGame(){

  userChoice = prompt("Please enter one (1) of 'Rock' OR 'Paper' OR 'Scissors' OR 'Dynamite'").toUpperCase();

  gamePieces = ['ROCK','PAPER','SCISSORS','DYNAMITE'];
  computerChoice = gamePieces[getRandomGamePiece(gamePieces.length)];
  whoWins();
}

// Game rules
function whoWins(){
  var results = "";

  if (userChoice==computerChoice){
    results = " It's a tie...Try again!";
  }else if(userChoice=="ROCK" && computerChoice=="PAPER"){
    results = " Loser!";
  }else if(userChoice=="ROCK" && computerChoice=="DYNAMITE"){
    results = " Loser!";
  }else if(userChoice=="ROCK" && computerChoice=="SCISSORS"){
    results = " You Win!";
  }else if(userChoice=="PAPER" && computerChoice=="DYNAMITE"){
    results = " Loser!";
  }else if(userChoice=="PAPER" && computerChoice=="SCISSORS"){
    results = " Loser!";
  }else if(userChoice=="SCISSORS" && computerChoice=="DYNAMITE"){
    results = " You Win!";
  }else if(userChoice=="PAPER" && computerChoice=="ROCK"){
    results = " You Win!"
  }else if(userChoice=="SCISSORS" && computerChoice=="PAPER"){
    results = " You Win!"
  }else if(userChoice=="SCISSORS" && computerChoice=="ROCK"){
    results = " Loser!"
  }else if(userChoice=="DYNAMITE" && computerChoice=="ROCK"){
    results = " You Win!"
  }else if(userChoice=="DYNAMITE" && computerChoice=="PAPER"){
    results = " You Win!"
  }else if(userChoice=="DYNAMITE" && computerChoice=="SCISSORS"){
    results = " Loser!"
  }else results = "Not a valid entry...Try again!";

          $("#results").html("User: "+userChoice + "<br/>"+ " VS "+ "<br/>" + "Computer: "+computerChoice + "<br/>"+ "<br/>"+results);
}
